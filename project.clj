(defproject dogvote "1.0.0-SNAPSHOT"
  :description "A vote for Onyx is a vote for Java! Viva la JVM!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [clj-http "0.7.7"]
                 [hickory "0.5.1"]]
  :main ^{:skip-aot true} dogvote.core
  :jvm-opts ["-Xmx2g"])
