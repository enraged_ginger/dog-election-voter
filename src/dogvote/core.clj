(ns dogvote.core
  (:require [clj-http.client :as client] ;used for http requests
            [hickory.core :as hickory]   ;used for parsing html
            [hickory.select :as s])      ;used for running html selectors
  (:gen-class :main true)) ;generate a CLI main class

;builds the form params for a vote
(defn build-form-params [token]
  {
   :token token
   :entry.479476178 "Onyx & Java"
})

;reads the user's authentication cookie in from a file and strips
;any new line characters from the end
(def cookie (clojure.string/replace (slurp "cookie.txt") "\n" ""))

;requests the voting form from Google, pulls the body element,
;and parses it using hickory
(defn request-form []
  (hickory/as-hickory
    (hickory/parse
      ((client/get "https://docs.google.com/a/nerdery.com/forms/d/1_Y-z2SEKgj5zntI5cEBSVmvQz4rcAIAkYYrQvUmgW80/viewform"
         {:headers {"cookie" cookie}}
        )
       :body
      )
    )
  )
)

;pulls the first HTML form element from the hickory parsed document
(defn get-form [parsed-doc]
  (first (s/select (s/tag :form) parsed-doc))
)

;pulls the action/form submission uri from the form
(defn get-form-action [form]
  ((form :attrs) :action)
)

;pulls the secret token attribute from the form
(defn get-form-token [form]
  (((first (s/select (s/attr :name #(= % "token")) form)) :attrs) :value)
)

;posts a vote to the action uri using the parameter token
(defn post-vote [action token]
  (client/post action
    {:form-params (build-form-params token)
     :headers {"cookie" cookie}
}))

;Performs a single vote by requesting the voting form,
;parsing the voting form, and then submitting the vote.
(defn do-vote []
  (let [form (get-form (request-form))
        action (get-form-action form)
        token (get-form-token form)]
    (post-vote action token)
  )
)

;The main method for this namespace.
;Starts up "threadCountStr" number of threads and asks each one
;to perform "actionCountStr" number of votes.
;If a failure occurs, the thread will sleep for a random number
;of seconds before trying again
(defn -main
  [threadCountStr actionCountStr & otheruselessargs]
  (dotimes [threadIndex (read-string threadCountStr)]
    (.start
      (Thread.
        (fn []
          (dotimes [iterationIndex (read-string actionCountStr)]
          (try
            (do-vote)
          (catch Exception e
            (do
              (println (str "Caught exception: " (.toString e)))
              (println (str "Gonna lay low for a bit. Thread:" threadIndex))
              (Thread/sleep (rand 5000))))))))))
)
