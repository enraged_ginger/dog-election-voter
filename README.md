# dogvote

Votes for Onyx! A vote for Onyx is a vote for Java! Viva la JVM!

## Usage

Vote for Onyx by either invoking do-vote in the dogvote.core namespace or invoke the uberjar from the command line:

java -jar dogvote-1.0.0-SNAPSHOT.jar x y

Where x is the number of threads that you wish to run and y is the number of votes you want each thread to cast.

Please note that your Google session cookie must be stored in a filed called cookie.txt in the directory that you run the project from.

## License

Copyright © 2013 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
